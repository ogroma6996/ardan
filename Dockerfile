FROM golang:1.16.3-alpine

WORKDIR /go/src/app
COPY ./app .
RUN go build -o ./app .
EXPOSE 8080
CMD ["./app"]
