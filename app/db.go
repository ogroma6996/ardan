package main

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"net/http"
	"os"
)

var schema = `
CREATE TABLE IF NOT EXISTS admins (
	id uuid unique not null,
	login varchar unique not null,
	pass varchar not null
);

CREATE TABLE IF NOT EXISTS sessions (
	id SERIAL,
	login varchar not null,
	token varchar not null
);
-- TODO this is super user creation
INSERT INTO admins(id, login, pass) VALUES('97690635-021f-459d-a6fd-42d364a8b856', 'root', '4813494d137e1631bba301d5acab6e7bb7aa74ce1185d456565ef51d737677b2')
ON CONFLICT DO NOTHING;
`

var connStr = os.Getenv("connStr")

func GetDB() *sqlx.DB {
	db, err := sqlx.Connect("postgres", connStr)
	OnErrFatal(err)
	return db
}

func InitDB() {
	db := GetDB()
	db.MustExec(schema)
}

func WriteAdmin(admin Admin) error {
	db := GetDB()
	_, err := db.Query(`INSERT INTO admins(id, login, pass) VALUES($1, $2, $3)`, admin.ID, admin.Login, admin.Pass)
	return err
}

func GetAdmin(login string) (Admin, error) {
	db := GetDB()
	admin := Admin{}
	err := db.Get(&admin, `SELECT * FROM admins WHERE login=$1`, login)
	OnErr(err)
	return admin, err
}

func WriteSession(c *http.Cookie, admin Admin) error {
	db := GetDB()
	_, err := db.Query(`INSERT INTO sessions(login, token) VALUES($1, $2) ON CONFLICT DO NOTHING`, admin.Login, c.Value)
	return err
}

func CheckSession(value string) bool {
	db := GetDB()
	session := Session{}
	err := db.Get(&session, `SELECT login, token FROM sessions WHERE token=$1`, value)
	OnErr(err)
	return session.Token == value
}
