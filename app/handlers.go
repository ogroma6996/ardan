package main

import (
	uuid "github.com/satori/go.uuid"
	"net/http"
)

func Unauthorized(w http.ResponseWriter) {
	w.WriteHeader(http.StatusUnauthorized)
	_, err := w.Write([]byte("Please log in first"))
	OnErr(err)
}

func SessionMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		cookie, err := r.Cookie("session")
		if err != nil || !CheckSession(cookie.Value) {
			OnErr(err)
			Unauthorized(w)
			return
		} else {
			next(w, r)
		}
	}
}

func CreateAdminHandler(w http.ResponseWriter, r *http.Request) {
	admin := GetAdminFromRequest(w, r)
	err := admin.CreateAdmin()
	ReturnInternalError(w, err)
	w.WriteHeader(http.StatusCreated)
	_, err = w.Write([]byte("Admin has ben created"))
	OnErr(err)
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	admin := GetAdminFromRequest(w, r)
	if admin.isAdmin() {
		cookie := &http.Cookie{Name: "session", Value: uuid.NewV4().String(), MaxAge: 600}
		http.SetCookie(w, cookie)
		err := WriteSession(cookie, admin)
		ReturnInternalError(w, err)
		http.Redirect(w, r, "/", http.StatusSeeOther)
	} else {
		w.WriteHeader(http.StatusForbidden)
		_, err := w.Write([]byte("Not admin"))
		ReturnInternalError(w, err)
	}
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("Hello Admin!"))
	OnErr(err)
}
